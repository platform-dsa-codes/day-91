//{ Driver Code Starts
import java.util.*;
import java.lang.*;
import java.io.*;
class GFG {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t > 0) {
            String s = sc.next();
            Solution obj = new Solution();
            System.out.println(obj.reverseWords(s));
            t--;
        }
    }
}

// } Driver Code Ends


class Solution 
{
    // Function to reverse words in a given string.
    String reverseWords(String S)
    {
        // Split the string into words using dot as delimiter.
        String[] words = S.split("\\.");
        
        // Initialize a StringBuilder to construct the reversed string.
        StringBuilder reversed = new StringBuilder();
        
        // Iterate through the words array in reverse order.
        for (int i = words.length - 1; i >= 0; i--) {
            // Append each word to the StringBuilder followed by a dot.
            reversed.append(words[i]);
            // Append a dot if it's not the last word.
            if (i > 0)
                reversed.append(".");
        }
        
        // Convert StringBuilder to String and return.
        return reversed.toString();
    }
}
